const data = {
  lists: {
    "list-1": {
      id: "list-1",
      title: "User story",
      tickets: [],
    },
    "list-2": {
      id: "list-2",
      title: "New",
      tickets: [],
    },
    "list-3": {
      id: "list-3",
      title: "In Progress",
      tickets: [],
    },
    "list-4": {
      id: "list-4",
      title: "Ready for test",
      tickets: [],
    },
    "list-5": {
      id: "list-5",
      title: "Needs info",
      tickets: [],
    },
    "list-6": {
      id: "list-6",
      title: "Closed",
      tickets: [],
    },
  },
  listIds: ["list-1", "list-2", "list-3", "list-4", "list-5", "list-6"],
};

export default data;
