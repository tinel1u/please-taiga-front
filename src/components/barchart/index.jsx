import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { Panel } from "rsuite";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    title: {
      display: true,
      text: "Tickets by status",
    },
    legend: {
      display: false,
    },
  },
  scales: {
    y: {
      min: 0,
      max: 25,
      stepSize: 1,
    },
    x: {},
  },
};

export default function BarChart({ stats }) {
  return (
    <Panel bordered>
      <Bar options={options} data={stats} />
    </Panel>
  );
}
