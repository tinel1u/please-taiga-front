import { useState } from "react";
import { Collapse } from "@material-ui/core";

import InputTicket from "../input-ticket";

import "./styles.scss";

export default function InputContainer({ listId, type }) {
  const [open, setOpen] = useState(false);

  return (
    <div className="input-container">
      <Collapse in={open}>
        <InputTicket setOpen={setOpen} listId={listId} type={type} />
      </Collapse>
      <Collapse in={!open}>
        {listId === "list-1" && (
          <div className="input-content">
            <button onClick={() => setOpen(!open)}>+ Add Ticket</button>
          </div>
        )}
      </Collapse>
    </div>
  );
}
