import { useContext, useState } from "react";

import TextareaAutosize from "react-textarea-autosize";
import { DeleteOutline } from "@material-ui/icons";
import { Draggable } from "react-beautiful-dnd";

import boardApi from "../../utils/boardContext";

import "./styles.scss";

export default function Ticket({ ticket, index }) {
  const [open, setOpen] = useState(false);
  const [newTitle, setNewTitle] = useState(ticket.title);
  const { removeTicket, updateTicketTitle } = useContext(boardApi);

  const handleOnBlur = () => {
    setNewTitle(newTitle);
    updateTicketTitle(newTitle, ticket);
    setOpen(!open);
  };

  return (
    <Draggable draggableId={ticket._id} index={index}>
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.dragHandleProps}
          {...provided.draggableProps}
        >
          <div className="ticket-content">
            {open ? (
              <TextareaAutosize
                type="text"
                className="input-ticket-title"
                value={newTitle}
                onChange={(e) => setNewTitle(e.target.value)}
                onBlur={handleOnBlur}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    handleOnBlur();
                  }
                  return;
                }}
                autoFocus
              />
            ) : (
              <div style={{ width: "100%" }}>
                <div
                  onClick={() => setOpen(!open)}
                  className="ticket-title-container"
                >
                  <p>{ticket.title}</p>
                  <button
                    onClick={() => {
                      removeTicket(ticket._id);
                    }}
                  >
                    <DeleteOutline />
                  </button>
                </div>
                <div className="ticket-title-container">
                  <p>Ref: {ticket?.reference}</p>
                </div>
                <div className="ticket-title-container">
                  <p>Content: {ticket?.content}</p>
                </div>
                <div className="ticket-title-container">
                  <p>Creator: {ticket?.creator}</p>
                </div>
              </div>
            )}
          </div>
        </div>
      )}
    </Draggable>
  );
}
