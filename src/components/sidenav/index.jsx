import { Sidenav, Nav } from "rsuite";
import DashboardIcon from "@rsuite/icons/Dashboard";
import TaskIcon from "@rsuite/icons/Task";
import "rsuite/dist/rsuite.min.css";

const styles = {
  display: "inline-table",
  marginRight: 10,
};

const SideNav = ({
  appearance,
  openKeys,
  expanded,
  onOpenChange,
  onExpand,
  activeKey,
  setActiveKey,
  setOpenKeys,
  setExpand,
  ...navProps
}) => {
  return (
    <div style={styles}>
      <Sidenav
        style={{ height: "100vh" }}
        openKeys={openKeys}
        onSelect={setActiveKey}
        onOpenChange={setOpenKeys}
        expanded={expanded}
      >
        <Sidenav.Body>
          <Nav {...navProps}>
            <Nav.Item eventKey="1" icon={<TaskIcon />} href="/ticketing">
              Ticketing
            </Nav.Item>
            <Nav.Item eventKey="2" icon={<DashboardIcon />} href="/dashboard">
              Dashboard
            </Nav.Item>
          </Nav>
        </Sidenav.Body>
        <Sidenav.Toggle onToggle={onExpand} />
      </Sidenav>
    </div>
  );
};

export default SideNav;
