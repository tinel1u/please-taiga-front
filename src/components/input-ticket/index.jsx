import { useContext, useState } from "react";
import { Clear } from "@material-ui/icons";

import boardApi from "../../utils/boardContext";

import "./styles.scss";

export default function InputTicket({ setOpen, listId, type }) {
  const { addMoreTicket } = useContext(boardApi);
  const [title, setTitle] = useState("");
  const [reference, setReference] = useState("");
  const [content, setContent] = useState("");
  // const [status, setStatus] = useState("");

  const handleTitleChange = (e) => {
    setTitle(e.target.value);
  };

  const handleReferenceChange = (e) => {
    setReference(e.target.value);
  };

  const handleContentChange = (e) => {
    setContent(e.target.value);
  };

  /*const handleStatusChange = (e) => {
    setStatus(e.target.value);
  };*/

  const handleBtnConfirm = () => {
    addMoreTicket(title, reference, content);
    setOpen(false);
    setTitle("");
  };

  return (
    <div className="input-ticket">
      <div className="input-ticket-container">
        <textarea
          onChange={handleTitleChange}
          value={title}
          className="input-text"
          placeholder="Title"
          autoFocus
        />
        <textarea
          onChange={handleReferenceChange}
          value={reference}
          className="input-text"
          placeholder="Reference"
          autoFocus
        />
        <textarea
          onChange={handleContentChange}
          value={content}
          className="input-text"
          placeholder="Content"
          autoFocus
        />
      </div>
      <div className="confirm">
        <button className="button-confirm" onClick={handleBtnConfirm}>
          Add Ticket
        </button>
        <button
          className="button-cancel"
          onClick={() => {
            setTitle("");
            setOpen(false);
          }}
        >
          <Clear />
        </button>
      </div>
    </div>
  );
}
