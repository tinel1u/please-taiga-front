import { Droppable } from "react-beautiful-dnd";

import Title from "../title";
import Ticket from "../ticket";
import InputContainer from "../input-container";

import "./styles.scss";

export default function List({ list, index }) {
  return (
    <div>
      <div className="list-tickets">
        <div className="title-list">
          <Title title={list.title} listId={list.id} />
        </div>
        <div className="container-tickets">
          <Droppable droppableId={list.id} type="task">
            {(provided) => (
              <div
                ref={provided.innerRef}
                {...provided.droppableProps}
                className="ticket-container"
              >
                {list.tickets.map((ticket, index) => (
                  <Ticket
                    key={ticket._id}
                    ticket={ticket}
                    index={index}
                    listId={list.id}
                  />
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </div>
        <InputContainer listId={list.id} type="ticket" />
      </div>
    </div>
  );
}
