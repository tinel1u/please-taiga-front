import { Navbar, Nav } from "rsuite";
import ExitIcon from "@rsuite/icons/Exit";

export default function NavBar() {
  const logout = () => {
    localStorage.clear();
    window.location.href = "/login";
  };

  return (
    <div className="show-fake-browser navbar-page">
      <Navbar style={{ background: "#0e050e" }}>
        <Navbar.Brand>
          <div style={{ marginLeft: "50px", color: "white" }}>PLEASE-TAIGA</div>
        </Navbar.Brand>
        <Nav pullRight>
          <Nav.Item
            style={{ color: "white" }}
            onClick={logout}
            icon={<ExitIcon />}
          ></Nav.Item>
        </Nav>
      </Navbar>
    </div>
  );
}
