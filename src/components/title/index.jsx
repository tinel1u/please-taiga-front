import { useState } from "react";

import "./styles.scss";

export default function Title({ title, listId }) {
  const [open] = useState(false);
  const [newTitle] = useState(title);

  return (
    <>
      {open ? (
        <div>
          <input
            type="text"
            className="input-title"
            disabled
            value={newTitle}
          />
        </div>
      ) : (
        <div className="editable-title-container">
          <h2 className="editable-title">{title}</h2>
        </div>
      )}
    </>
  );
}
