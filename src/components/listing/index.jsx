import _ from "lodash";
import { List, FlexboxGrid, Panel } from "rsuite";
import "./styles.scss";

export default function Listing({ tickets }) {
  return (
    <div>
      <Panel bordered>
        <List size="sm" hover>
          {tickets.map((item, index) => (
            <List.Item key={item["_id"]} index={index + 1}>
              <FlexboxGrid>
                {/*status data*/}
                <FlexboxGrid.Item colspan={5} className="styleCenter">
                  <div style={{ textAlign: "left", marginLeft: "5%" }}>
                    <div className="slimText">Status</div>
                    <div className="dataStyle">
                      {_.capitalize(item["status"].toLocaleString())}
                    </div>
                  </div>
                </FlexboxGrid.Item>
                {/*reference data*/}
                <FlexboxGrid.Item colspan={6} className="styleCenter">
                  <div style={{ textAlign: "left" }}>
                    <div className="slimText">Reference</div>
                    <div className="dataStyle">
                      {_.capitalize(item["reference"].toLocaleString())}
                    </div>
                  </div>
                </FlexboxGrid.Item>
                {/*title data*/}
                <FlexboxGrid.Item colspan={6} className="styleCenter">
                  <div style={{ textAlign: "left" }}>
                    <div className="slimText">Title</div>
                    <div className="dataStyle">
                      {_.capitalize(item["title"].toLocaleString())}
                    </div>
                  </div>
                </FlexboxGrid.Item>
                {/*creator data*/}
                <FlexboxGrid.Item colspan={5} className="styleCenter">
                  <div style={{ textAlign: "left" }}>
                    <div className="slimText">Creator</div>
                    <div className="dataStyle">
                      {_.capitalize(item["creator"].toLocaleString())}
                    </div>
                  </div>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </List.Item>
          ))}
        </List>
      </Panel>
    </div>
  );
}
