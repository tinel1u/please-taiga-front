import SideNav from "../sidenav";
import React from "react";
import { Container, Header, Content, Sidebar } from "rsuite";
import NavBar from "../navbar";

export default function AppLayout(props) {
  const [activeKey, setActiveKey] = React.useState();
  const [openKeys, setOpenKeys] = React.useState([]);
  const [expanded, setExpand] = React.useState(false);

  return (
    <div className="show-container">
      <Container>
        <Header>
          <NavBar />
        </Header>
        <Container style={{ height: "100vh - 56px" }}>
          <Sidebar width={expanded ? 154 : 56}>
            <SideNav
              activeKey={activeKey}
              openKeys={openKeys}
              onSelect={setActiveKey}
              onOpenChange={setOpenKeys}
              expanded={expanded}
              onExpand={setExpand}
            />
          </Sidebar>
          <Content>{props.children}</Content>
        </Container>
      </Container>
    </div>
  );
}
