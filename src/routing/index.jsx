import { Route, Routes } from "react-router-dom";
import AppLayout from "../components/layout";
import Login from "../pages/login";

function hasJWT() {
  return localStorage.getItem("token");
}

const RouterCombiner = ({ routes }) => {
  const RoutesMap = routes.map(({ Component, path, redirect }) => {
    const ComponentWithLayout = <AppLayout children={Component}></AppLayout>;
    return [
      <Route
        key={path}
        element={hasJWT() ? ComponentWithLayout : <Login />}
        path={path}
        redirect={redirect}
      />,
    ];
  });
  return <Routes>{RoutesMap}</Routes>;
};

export default RouterCombiner;
