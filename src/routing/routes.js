import Ticketing from "../pages/ticketing";
import Dashboard from "../pages/dashboard";

const routes = [
  {
    path: "/",
    Component: (
      <div
        style={{
          paddingLeft: "10px",
          paddingTop: "15px",
          color: "white",
          fontSize: "35px",
        }}
      >
        Welcome to Please-Taiga App
      </div>
    ),
    redirect: "http://localhost:3000/api#/",
  },
  {
    path: "/dashboard",
    exact: true,
    Component: <Dashboard />,
  },
  {
    path: "/ticketing",
    exact: true,
    Component: <Ticketing />,
  },
  {
    path: "/login",
    Component: <Dashboard />,
  },
  {
    path: "*",
    Component: <div>404 NotFound</div>,
  },
];

export default routes;
