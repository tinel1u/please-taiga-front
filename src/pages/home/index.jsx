import "../../index.scss";
import "./styles.scss";

import { BrowserRouter as Router } from "react-router-dom";
import routes from "../../routing/routes.js";
import RouterCombiner from "../../routing";
import axios from "axios";

axios.defaults.headers.common = {
  Authorization: "Bearer " + localStorage.getItem("token"),
};

export default function Home() {
  const auth = false;
  return (
    <div>
      <Router>
        <RouterCombiner routes={routes} auth={auth} />
      </Router>
    </div>
  );
}
