import React, { useState } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import List from "../../components/list";
import boards from "../../utils/boards";
import BoardApi from "../../utils/boardContext";
import "./styles.scss";
import ticketService from "../../services/ticket.service";
import statuses from "../../enums/statuses";
import Input from "rsuite/Input";
import { useToaster, Message } from "rsuite";
import ticketStatusesEnum from "../../enums/statuses";

export default function Ticketing() {
  const toaster = useToaster();
  const [tickets, setTickets] = React.useState([]);
  const [filteredTickets, setFilteredTickets] = React.useState([]);
  const [title] = useState("");
  const [reference] = useState("");

  const fetchData = () =>
    ticketService
      .getTickets()
      .then((res) => {
        const tickets = res.data;
        setTickets(tickets);
        setFilteredTickets(tickets);
      })
      .catch((error) => {
        toaster.push(
          <Message showIcon type="error" header="Error">
            {error.message}
          </Message>,
          {
            placement: "topEnd",
          }
        );
      });

  React.useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const [data] = useState(boards);

  const addMoreTicket = (title, reference, content) => {
    if (!title) {
      return;
    }

    const payload = {
      title: title,
      reference: reference,
      content: content,
      status: ticketStatusesEnum["user story"].status,
    };

    ticketService
      .createTicket(payload)
      .then(() => {
        ticketService.getTickets().then((res) => {
          setTickets(res.data);
          setFilteredTickets(res.data);
        });
      })
      .catch((error) => {
        toaster.push(
          <Message showIcon type="error" header="Error">
            {error.message}
          </Message>,
          {
            placement: "topEnd",
          }
        );
      });
  };

  const updateTicketTitle = (title, ticket) => {
    ticketService
      .patchTicket(ticket._id, { title: title })
      .then(() => {
        ticketService
          .getTickets()
          .then((res) => {
            setFilteredTickets(res.data);
            setTickets(res.data);
          })
          .catch((error) => {
            toaster.push(
              <Message showIcon type="error" header="Error">
                {error.message}
              </Message>,
              {
                placement: "topEnd",
              }
            );
          });
      })
      .catch((error) => {
        toaster.push(
          <Message showIcon type="error" header="Error">
            {error.message}
          </Message>,
          {
            placement: "topEnd",
          }
        );
      });
  };

  const removeTicket = (id) => {
    ticketService.deleteTicket(id).then(() => {
      ticketService
        .getTickets()
        .then((res) => {
          setTickets(res.data);
          setFilteredTickets(res.data);
        })
        .catch((error) => {
          toaster.push(
            <Message showIcon type="error" header="Error">
              {error.message}
            </Message>,
            {
              placement: "topEnd",
            }
          );
        })
        .catch((error) => {
          toaster.push(
            <Message showIcon type="error" header="Error">
              {error.message}
            </Message>,
            {
              placement: "topEnd",
            }
          );
        });
    });
  };

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    const sourceList = data.lists[source.droppableId];
    const destinationList = data.lists[destination.droppableId];
    const draggingTicket = sourceList.tickets.filter(
      (ticket) => ticket._id === draggableId
    )[0];

    const status = Object.values(statuses).filter(
      (status) => status.id === destinationList.id
    );

    ticketService
      .patchTicket(draggingTicket._id, { status: status[0]?.status })
      .then(() => {
        ticketService.getTickets().then((res) => {
          setTickets(res.data);
          setFilteredTickets(res.data);
        });
      })
      .catch((error) => {
        toaster.push(
          <Message showIcon type="error" header="Error">
            {error.message}
          </Message>,
          {
            placement: "topEnd",
          }
        );
      });
  };

  const inputHandler = (event) => {
    var lowerCase = event.toLowerCase().trim();
    const filteredTickets = tickets.filter((ticket) => {
      if (lowerCase === "") {
        return ticket;
      } else {
        return (
          ticket?.title.toLowerCase().includes(lowerCase) ||
          ticket?.reference.toLowerCase().includes(lowerCase) ||
          ticket?.content.toLowerCase().includes(lowerCase) ||
          ticket?.creator.toLowerCase().includes(lowerCase)
        );
      }
    });
    setFilteredTickets(filteredTickets);
  };

  return (
    <div className="container">
      <BoardApi.Provider
        value={{
          addMoreTicket,
          removeTicket,
          updateTicketTitle,
        }}
      >
        <div className="main">
          <div className="search">
            <Input
              style={{ height: "5vh", width: "100%" }}
              id="outlined-basic"
              onChange={inputHandler}
              label="Search"
              placeholder="Search..."
            />
          </div>
        </div>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="app" type="list" direction="horizontal">
            {(provided) => (
              <div
                className="wrapper"
                ref={provided.innerRef}
                {...provided.droppableProps}
              >
                {data.listIds.map((listId, index) => {
                  const list = data.lists[listId];
                  list.tickets = filteredTickets.filter(
                    (ticket) => statuses[ticket.status]?.id === list.id
                  );

                  return (
                    <List
                      title={title}
                      reference={reference}
                      list={list}
                      key={listId}
                      index={index}
                    />
                  );
                })}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </BoardApi.Provider>
    </div>
  );
}
