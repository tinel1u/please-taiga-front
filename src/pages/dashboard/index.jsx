import Listing from "../../components/listing";
import BarChart from "../../components/barchart";
import "../../index.scss";
import React from "react";
import dashboardService from "../../services/dashboard.service";
import statuses from "../../enums/statuses";
import { useToaster, Message } from "rsuite";
import ticketService from "../../services/ticket.service";
import "./styles.scss";

export default function Dashboard() {
  const toaster = useToaster();
  const roles = localStorage.getItem("roles");
  const labels = Object.values(statuses)
    .map((status) => status.statLabel)
    .filter((status) => status !== undefined);

  const [tickets, setTickets] = React.useState([]);
  const [stats, setStats] = React.useState({
    labels,
    datasets: [
      {
        data: [],
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
    ],
  });

  const fetchData = () =>
    dashboardService
      .getTicketsByStatus()
      .then((res) => {
        const stats = res.data;
        setStats({
          labels,
          datasets: [
            {
              data: Object.values(stats),
              backgroundColor: "rgba(255, 99, 132, 0.5)",
            },
          ],
        });
      })
      .catch((error) => {
        toaster.push(
          <Message showIcon type="error" header="Error">
            {error.message}
          </Message>,
          {
            placement: "topEnd",
          }
        );
      });

  React.useEffect(() => {
    if (localStorage.getItem("roles").includes("admin")) {
      fetchData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    ticketService.getTickets().then((res) => {
      const tickets = res.data;
      setTickets(tickets);
    });
  }, []);

  return (
    <div className="container">
      {roles.includes("admin") && (
        <div style={{ width: "30%", marginLeft: "9px" }}>
          <BarChart stats={stats} />
        </div>
      )}
      <div style={{ width: "68%", marginLeft: "9px" }}>
        <Listing tickets={tickets} />
      </div>
    </div>
  );
}
