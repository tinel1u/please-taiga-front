import React, { useEffect } from "react";
import {
  Container,
  ButtonToolbar,
  Button,
  Form,
  Content,
  FlexboxGrid,
  Panel,
  Schema,
  Message,
  useToaster,
} from "rsuite";
import AuthService from "../../services/auth.service";

const model = Schema.Model({
  password: Schema.Types.StringType().isRequired("This field is required."),
  email: Schema.Types.StringType().isEmail(
    "Please enter a valid email address."
  ),
});

function TextField(props) {
  const { name, label, accepter, ...rest } = props;
  return (
    <Form.Group controlId={`${name}-3`}>
      <Form.ControlLabel>{label} </Form.ControlLabel>
      <Form.Control name={name} accepter={accepter} {...rest} />
    </Form.Group>
  );
}
export default function Login() {
  const toaster = useToaster();
  const formRef = React.useRef();
  const [login, setLogin] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [formValue, setFormValue] = React.useState({
    email: "",
    password: "",
  });

  useEffect(() => {
    setLogin("jdoe@mail.com");
    setPassword("test");
  }, []);

  const handleSubmit = () => {
    // if (!formRef.current.check()) {
    //   console.error("Form Error");
    //   return;
    // }

    AuthService.login(login, password).then(
      () => {
        toaster.push(
          <Message showIcon type="success" header="Success">
            Authentication successful
          </Message>,
          {
            placement: "topEnd",
          }
        );
        window.location.href = "/ticketing";
      },
      () => {
        toaster.push(
          <Message showIcon type="error" header="Error">
            Authentication failed
          </Message>,
          {
            placement: "topEnd",
          }
        );
        return;
      }
    );
  };

  return (
    <div className="show-fake-browser login-page">
      <Container>
        <Content style={{ marginTop: "150px" }}>
          <FlexboxGrid justify="center">
            <FlexboxGrid.Item colspan={12}>
              <Panel header={<h3>Login</h3>} bordered>
                <Form
                  ref={formRef}
                  onChange={setFormValue}
                  formValue={formValue}
                  model={model}
                  fluid
                >
                  <TextField
                    name="email"
                    label="Email"
                    placeholder="jdoe@mail.com"
                  />
                  <TextField
                    name="password"
                    label="Password"
                    type="password"
                    placeholder="test"
                    autoComplete="off"
                  />

                  <ButtonToolbar>
                    <Button appearance="primary" onClick={handleSubmit}>
                      Submit
                    </Button>
                  </ButtonToolbar>
                </Form>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
      </Container>
    </div>
  );
}
