import axios from "axios";
const DASHBOARD_CONTROLLER_API_URL = "dashboard";

class DashboardService {
  getTicketsByStatus() {
    return axios
      .get(process.env.REACT_APP_API_URL + DASHBOARD_CONTROLLER_API_URL)
      .then((response) => {
        return response.data;
      });
  }
}
export default new DashboardService();
