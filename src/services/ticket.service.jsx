import axios from "axios";

const TICKET_CONTROLLER_API_URL = "tickets";

class TicketService {
  getTickets() {
    return axios
      .get(process.env.REACT_APP_API_URL + TICKET_CONTROLLER_API_URL)
      .then((response) => {
        return response.data;
      });
  }

  patchTicket(id, patchPayload) {
    return axios
      .patch(
        process.env.REACT_APP_API_URL + TICKET_CONTROLLER_API_URL + "/" + id,
        patchPayload
      )
      .then((response) => {
        return response.data;
      });
  }

  createTicket(createPayload) {
    return axios
      .post(
        process.env.REACT_APP_API_URL + TICKET_CONTROLLER_API_URL,
        createPayload
      )
      .then((response) => {
        return response.data;
      });
  }

  deleteTicket(id) {
    return axios
      .delete(
        process.env.REACT_APP_API_URL + TICKET_CONTROLLER_API_URL + "/" + id
      )
      .then((response) => {
        return response.data;
      });
  }
}

export default new TicketService();
