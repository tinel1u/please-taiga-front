import axios from "axios";
const AUTH_CONTROLLER_API_URL = "auth";
class AuthService {
  login(email, password) {
    return axios
      .post(
        process.env.REACT_APP_API_URL + AUTH_CONTROLLER_API_URL + "/login",
        {
          email,
          password,
        }
      )
      .then((response) => {
        if (response.data.token) {
          localStorage.setItem("token", response.data.token);
          localStorage.setItem("roles", response.data.roles);
        }
        return;
      });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem("user"));
  }
}
export default new AuthService();
