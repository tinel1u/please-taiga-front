const ticketStatusesEnum = {
  "user story": {
    id: "list-1",
    status: "user story",
  },
  new: {
    id: "list-2",
    status: "new",
    statLabel: "New",
  },
  "in progress": {
    id: "list-3",
    status: "in progress",
    statLabel: "In progress",
  },
  "ready for test": {
    id: "list-4",
    status: "ready for test",
    statLabel: "Ready for test",
  },
  "needs info": {
    id: "list-5",
    status: "needs info",
    statLabel: "Needs info",
  },
  closed: {
    id: "list-6",
    status: "closed",
    statLabel: "Closed",
  },
};

export default ticketStatusesEnum;
